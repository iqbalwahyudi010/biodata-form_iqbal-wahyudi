function kirim(){
    var nama = document.getElementById("nama").value;
    var tanggal = document.getElementById("tanggal").value;
    var nim = document.getElementById("nim").value;
    var prodi = document.getElementById("prodi").value;
    var kelamin = document.getElementById("kelamin").value;
    var telepon = document.getElementById("telepon").value;
    var alamat = document.getElementById("alamat").value;
    var email = document.getElementById("email").value;

    if (nama == "" || tanggal == "" || nim == "" || prodi == "" || telepon == "" || alamat == "" || email == "") {
        alert("Harap isi semua form!");
    } else {
        const emails = "gmail.com";
        var namas = /^[A-Za-z\s]+$/;
        var nims = /^\d{10}$/;
        var prodis = /^[A-Za-z\s]+$/;
        var telepons = /^\d+$/;

        if (!namas.test(nama)) {
            alert("Harap nama diisi dengan benar!");
        } else if (!nims.test(nim)) {
            alert("Harap NIM 10 digit dan menggunakan angka!");
        } else if (!prodis.test(prodi)) {
            alert("Harap Prodi diisi dengan benar!");
        } else if (!telepons.test(telepon)) {
            alert("Harap Nomor Telepon diisi dengan benar!");
        } else if(!email.endsWith(emails)){
            alert("Harap email menggunakan email berdomain Google!");
         }else{
            if (confirm("Apakah form sudah terisi dengan benar ?")){
                alert(
                    "Nama : " + nama + 
                    "\nTanggal Lahir : " + tanggal +
                    "\nNIM : " + nim +
                    "\nProgram Studi : " + prodi +
                    "\nJenis Kelamin : " + kelamin +
                    "\nNo.Hp : " + telepon +
                    "\nAlamat : " + alamat +
                    "\nEmail : " + email
                );
            }
            }
        }
    }

document.getElementById("kirim").addEventListener("click", function(event){
    kirim();
    event.preventDefault()
});

function hapus(){
    if(confirm("Apakah anda yakin ingin menghapus semua ?")){
        document.getElementById("nama").value = "";
        document.getElementById("tanggal").value = "";
        document.getElementById("nim").value = "";
        document.getElementById("prodi").value = "";
        document.getElementById("kelamin").value = "";
        document.getElementById("telepon").value = "";
        document.getElementById("alamat").value = "";
        document.getElementById("email").value = "";
    }
}

document.getElementById("hapus").addEventListener("click", function(event){
    hapus();
    event.preventDefault()
});